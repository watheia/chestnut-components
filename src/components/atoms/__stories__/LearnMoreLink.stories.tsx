import React from "react"
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from "@storybook/react/types-6-0"
import LearnMoreLink from "../LearnMoreLink"

export default {
  title: "atoms/LearnMoreLink",
  component: LearnMoreLink,
} as Meta

const Template: Story<LearnMoreLinkProps> = (args) => <LearnMoreLink {...args} />

export const Primary = Template.bind({})
