import React from "react"
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from "@storybook/react/types-6-0"
import Icon from "../Icon"

export default {
  title: "atoms/Icon",
  component: Icon,
} as Meta

const Template: Story<IconProps> = (args) => <Icon {...args} />

export const usingFontIconClass = Template.bind({ fontIconClass: "email" })
