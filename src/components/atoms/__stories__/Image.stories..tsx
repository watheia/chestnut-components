import React from "react"
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from "@storybook/react/types-6-0"
import Image from "../Image"

export default {
  title: "atoms/Image",
  component: Image,
} as Meta

const Template: Story<ImageProps> = (args) => <Image {...args} />

export const usingSrc = Template.bind({ src: "https://cdn.watheia.org/assets/icon.png" })
