import React from "react"
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from "@storybook/react/types-6-0"
import ScrollTop from "../ScrollTop"

export default {
  title: "atoms/ScrollTop",
  component: ScrollTop,
} as Meta

const Template: Story = () => <ScrollTop />

export const Primary = Template.bind({})
