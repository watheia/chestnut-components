import React from "react"
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from "@storybook/react/types-6-0"
import { DarkModeToggler } from "../DarkModeToggler"
import { CssBaseline, Paper } from "@material-ui/core"

export default {
  title: "atoms/DarkModeToggler",
  component: DarkModeToggler,
} as Meta

function App({}) {
  const handleClick = () => {
    setState({ themeMode: "dark" })
  }

  const [state, setState] = React.useState<{ themeMode: "light" | "dark" }>({
    themeMode: "light",
  })

  return (
    <CssBaseline>
      <Paper>
        <DarkModeToggler themeMode={state.themeMode} onClick={handleClick} />
      </Paper>
    </CssBaseline>
  )
}

const Template: Story = (args) => <App {...args} />

export const usingClickHandler = Template.bind({})
