import React from "react"
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from "@storybook/react/types-6-0"
import IconText from "../IconText"

export default {
  title: "atoms/IconText",
  component: IconText,
} as Meta

const Template: Story<IconTextProps> = (args) => <IconText {...args} />

export const usingDefaults = Template.bind({
  fontIconClass: "email",
  title: "Hello, World!",
})
