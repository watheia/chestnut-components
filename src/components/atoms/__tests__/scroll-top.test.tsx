import * as React from "react"
import { render } from "@testing-library/react"
import ScrollTop from "../ScrollTop"

describe("atoms.ScrollTop", () => {
  it("should be rendered correctly", () => {
    const { asFragment } = render(<ScrollTop />)
    expect(asFragment()).toMatchSnapshot()
  })
})
