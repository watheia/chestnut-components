import * as React from "react"
import { render } from "@testing-library/react"
import LearnMoreLink from "../LearnMoreLink"

describe("atoms.LearnMoreLink", () => {
  it("should be rendered correctly", () => {
    const { asFragment } = render(<LearnMoreLink title="Hello, World!" />)
    expect(asFragment()).toMatchSnapshot()
  })
})
