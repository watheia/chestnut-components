import * as React from "react"
import { render } from "@testing-library/react"
import Icon from "../Icon"

describe("atoms.Icon", () => {
  it("should be rendered correctly", () => {
    const { asFragment } = render(<Icon fontIconClass="email" />)
    expect(asFragment()).toMatchSnapshot()
  })
})
