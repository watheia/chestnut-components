import CountUpNumber from "./CountUpNumber"
import DescriptionCta from "./DescriptionCta"
import IconAlternate from "./IconAlternate"
import OverlapedImages from "./OverlapedImages"
import SectionHeader from "./SectionHeader"
import SwiperImage from "./SwiperImage"
import SwiperNumber from "./SwiperNumber"
import TypedText from "./TypedText"

export {
  CountUpNumber,
  DescriptionCta,
  IconAlternate,
  OverlapedImages,
  SectionHeader,
  SwiperImage,
  SwiperNumber,
  TypedText,
}
export default {
  CountUpNumber,
  DescriptionCta,
  IconAlternate,
  OverlapedImages,
  SectionHeader,
  SwiperImage,
  SwiperNumber,
  TypedText,
}
