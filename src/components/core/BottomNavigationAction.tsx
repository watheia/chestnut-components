import MuiBottomNavigationAction, {
  BottomNavigationActionProps,
} from "@material-ui/core/BottomNavigationAction"

import patchButtonBaseComponent from "./patchButtonBaseComponent"

const BottomNavigationAction = patchButtonBaseComponent<BottomNavigationActionProps>(
  MuiBottomNavigationAction,
)

export default BottomNavigationAction
