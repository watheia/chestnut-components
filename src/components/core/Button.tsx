import MuiButton, { ButtonProps } from "@material-ui/core/Button"

import patchButtonBaseComponent from "./patchButtonBaseComponent"

const Button = patchButtonBaseComponent<ButtonProps>(MuiButton)
export default Button
