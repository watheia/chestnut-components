import MuiIconButton, { IconButtonProps } from "@material-ui/core/IconButton"

import patchButtonBaseComponent from "./patchButtonBaseComponent"

const IconButton = patchButtonBaseComponent<IconButtonProps>(MuiIconButton)
export default IconButton
