import MuiFab, { FabProps } from "@material-ui/core/Fab"

import patchButtonBaseComponent from "./patchButtonBaseComponent"

const Fab = patchButtonBaseComponent<FabProps>(MuiFab)
export default Fab
