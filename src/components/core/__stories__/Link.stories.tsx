import React from "react"
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from "@storybook/react/types-6-0"
import { LinkProps } from "@material-ui/core/Link"

const Link = () => (
  <div style={{ padding: "16px", backgroundColor: "#eeeeee" }}>
    <h1 style={{ color: "rebeccapurple" }}>Hello from Storybook and Gatsby!</h1>
  </div>
)

export default {
  title: "core/Link",
  component: Link,
} as Meta

const Template: Story<LinkProps> = (args) => <Link {...args} />
export const Primary = Template.bind({ href: "#" })
