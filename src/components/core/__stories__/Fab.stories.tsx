import React from "react"
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from "@storybook/react/types-6-0"
import { FabProps } from "@material-ui/core/Fab"

const Fab = () => (
  <div style={{ padding: "16px", backgroundColor: "#eeeeee" }}>
    <h1 style={{ color: "rebeccapurple" }}>Hello from Storybook and Gatsby!</h1>
  </div>
)

export default {
  title: "core/Fab",
  component: Fab,
} as Meta

const Template: Story<FabProps> = (args) => <Fab {...args} />

export const Primary = Template.bind({})
