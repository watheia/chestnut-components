import MuiCardActionArea, { CardActionAreaProps } from "@material-ui/core/CardActionArea"

import patchButtonBaseComponent from "./patchButtonBaseComponent"

const CardActionArea = patchButtonBaseComponent<CardActionAreaProps>(MuiCardActionArea)

export default CardActionArea
