import React from "react"
import ServerError from "views/ServerError"
import Minimal from "components/layout/Minimal"
import WithLayout from "components/layout/WithLayout"

const ErrorPage = (): JSX.Element => {
  return <WithLayout component={ServerError} layout={Minimal} />
}

export default ErrorPage
