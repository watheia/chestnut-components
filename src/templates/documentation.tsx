/**
 * Caution: Consider this file when using NextJS or GatsbyJS
 *
 * You may delete this file and its occurrences from the project filesystem if you are using react-scripts
 */
import React from "react"
import Documentation from "views/Documentation"
import DocsLayout from "components/layout/DocsLayout"
import WithLayout from "components/layout/WithLayout"

const DocumentationPage = (): JSX.Element => {
  return <WithLayout component={Documentation} layout={DocsLayout} />
}

export default DocumentationPage
