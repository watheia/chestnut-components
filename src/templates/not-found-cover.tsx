/**
 * Caution: Consider this file when using NextJS or GatsbyJS
 *
 * You may delete this file and its occurrences from the project filesystem if you are using react-scripts
 */
import React from "react"
import NotFoundCover from "views/NotFoundCover"
import Minimal from "components/layout/Minimal"
import WithLayout from "components/layout/WithLayout"

const NotFoundCoverPage = (): JSX.Element => {
  return <WithLayout component={NotFoundCover} layout={Minimal} />
}

export default NotFoundCoverPage
