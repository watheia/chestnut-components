/**
 * Caution: Consider this file when using NextJS or GatsbyJS
 *
 * You may delete this file and its occurrences from the project filesystem if you are using react-scripts
 */
import React from "react"
import CareerListing from "views/CareerListing"
import Main from "components/layout/Main"
import WithLayout from "components/layout/WithLayout"

const CareerListingPage = (): JSX.Element => {
  return <WithLayout component={CareerListing} layout={Main} />
}

export default CareerListingPage
