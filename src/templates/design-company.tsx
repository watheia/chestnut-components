import React from "react"
import DesignCompany from "views/DesignCompany"
import Main from "components/layout/Main"
import WithLayout from "components/layout/WithLayout"

const DesignCompanyPage = (): JSX.Element => {
  return <WithLayout component={DesignCompany} layout={Main} />
}

export default DesignCompanyPage
