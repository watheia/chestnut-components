interface AccoundionItemProps {
  id: number | string
  title: string
  subtitle: string
  text: string
  link: string
}

interface AccordionProps {
  /**
   * Classname from the parent component
   */
  className?: string
  /**
   * Items to show inside the accordion
   */
  items: Array<AccoundionItemProps>
  /**
   * Additional properties to pass to the title Typography component
   */
  titleProps?: Record<string, any>
  /**
   * Additional properties to pass to the subtitle Typography component
   */
  subtitleProps?: Record<string, any>
  /**
   * Additional properties to pass to the text Typography component
   */
  textProps?: Record<string, any>
  /**
   * Additional properties to pass to the link component
   */
  linkProps?: Record<string, any>
  // All other props
  [x: string]: any
}
