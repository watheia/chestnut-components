interface ViewComponentProps {
  className?: string
  data?: any
  themeMode?: "light" | "dark"
  // All other props
  [x: string]: any
}
